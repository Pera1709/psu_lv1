from os import X_OK, link
from funkcija_5_1 import *
from scipy.cluster.hierarchy import dendrogram, linkage
import matplotlib.pylab as plt


X = generate_data(50, 3)

Z = linkage(X, 'single')
fig = plt.figure(figsize=(25, 10))
dn = dendrogram(Z)

Z = linkage(X, 'ward')
fig = plt.figure(figsize=(25, 10))
dn = dendrogram(Z)

plt.show()
