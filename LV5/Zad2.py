from funkcija_5_1 import *
import sklearn.cluster as clstr
import matplotlib.pylab as plt


data = generate_data(500, 1)

inertias = []
for i in range(1, 20):
    model = clstr.KMeans(n_clusters=i)
    model.fit(data)
    inertias.append(model.inertia_)

plt.figure()
plt.plot(range(1, 20), inertias)
plt.show()
