import matplotlib.image as mpimg 
import matplotlib.pylab as plt
import sklearn.cluster as clstr
import numpy as np


image = mpimg.imread("LV5/example_grayscale.png")

for i in range(2, 6):
    means = clstr.KMeans(n_clusters=i, n_init=1)
    means.fit(image.reshape((-1, 1))) 
    values = means.cluster_centers_.squeeze()
    labels = means.labels_
    newImageComp = np.choose(labels, values)
    newImageComp.shape = image.shape

    plt.figure(i)
    plt.imshow(newImageComp,  cmap='gray')

plt.show()
