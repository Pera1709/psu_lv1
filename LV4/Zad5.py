import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pyparsing import line
import seaborn as sns

import sklearn.linear_model as lm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error, max_error


cars = pd.read_csv('LV4/cars_processed.csv')
cars = cars.drop(['name', 'mileage'], axis=1)

X = cars[['km_driven', 'year', 'engine', 'max_power']]
Y = cars[['selling_price']]

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=10)

Scaler = MinMaxScaler()
X_train_s = Scaler.fit_transform(X_train)
X_test_s = Scaler.transform(X_test)

linear_model = lm.LinearRegression()
linear_model.fit(X_train_s, Y_train)

Y_pred_train = linear_model.predict(X_train_s)
Y_pred_test = linear_model.predict(X_test_s)

print("R2 test: ", r2_score(Y_pred_test, Y_test))
print("RMSE test: ", np.sqrt(mean_squared_error(Y_pred_test, Y_test)))
print("Max error test: ", max_error(Y_pred_test, Y_test))
print("MAE test: ", mean_absolute_error(Y_pred_test, Y_test))
