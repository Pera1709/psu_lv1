import matplotlib.pyplot as plt
import numpy as np
import skimage.io as sio


def GenLine(sqr1, sqr2, len):
    line = sqr1
    wasFirstSqr = True
    for i in range(len - 1):
        if (wasFirstSqr):
            line = np.hstack((line, sqr2))
        else:
            line = np.hstack((line, sqr1))
        wasFirstSqr = not wasFirstSqr
    return line

def GetSqrs(h, w, sqrSize):
    wSqr = 255 * np.ones((sqrSize, sqrSize))
    bSqr = np.zeros((sqrSize, sqrSize))

    hLine1 = GenLine(wSqr, bSqr, w)
    hLine2 = GenLine(bSqr, wSqr, w)

    img = hLine1
    firstLineUsed = True
    for i in range(h - 1):
        if (firstLineUsed):
            img = np.vstack((img, hLine2))
        else:
            img = np.vstack((img, hLine1))
        firstLineUsed = not firstLineUsed

    return img

img = GetSqrs(10, 10, 50)

# show
plt.figure(1)
plt.imshow(img, cmap="gray", vmin=0, vmax=255)
plt.show()
