import matplotlib.pyplot as plt
import numpy as np
import skimage.io as sio
    

# open
img = sio.imread("LV2/tiger.png", as_gray=True)

# a
brightVal = 45
imgBri = img.copy()
for y in range(img.shape[0]):
    for x in range(img.shape[1]):
        if (img[y, x] <= (255 - brightVal)):
            imgBri[y, x] = img[y, x] + brightVal
        else:
            imgBri[y, x] = 255

# b
imgRot = np.zeros((img.shape[1], img.shape[0]))
for y in range(img.shape[0]):
    for x in range(img.shape[1]):
        imgRot[x, y] = img[img.shape[0] - 1 - y, x]

# c
imgMir = img.copy()
for y in range(img.shape[0]):
    for x in range(img.shape[1]):
        imgMir[y, x] = img[y, img.shape[1] - 1 - x]

# d
min = 5
imgRes = np.zeros((img.shape[0] // min, img.shape[1] // min))
for y in range(imgRes.shape[0]):
    for x in range(imgRes.shape[1]):
        imgRes[y, x] = img[y * min, x * min]

# e
qrt1 = int(img.shape[1] / 4 * 2)
qrt2 = int(img.shape[1] / 4 * 3)
imgQrt = np.zeros(img.shape)
for y in range(img.shape[0]):
    for x in range(img.shape[1]):
        if (x > qrt1 and x < qrt2):
            imgQrt[y, x] = img[y, x]

# show
#plt.figure(1)
#plt.imshow(img, cmap="gray", vmin=0, vmax=255)
#plt.figure(2)
#plt.imshow(imgBri, cmap="gray", vmin=0, vmax=255)
#plt.figure(3)
#plt.imshow(imgRot, cmap="gray", vmin=0, vmax=255)
#plt.figure(4)
#plt.imshow(imgMir, cmap="gray", vmin=0, vmax=255)
#plt.figure(5)
#plt.imshow(imgRes, cmap="gray", vmin=0, vmax=255)
plt.figure(6)
plt.imshow(imgQrt, cmap="gray", vmin=0, vmax=255)
plt.show()
