import numpy as np 
import matplotlib.pyplot as plt


data = np.loadtxt(open("LV2/mtcars.csv", "rb"), usecols=(1, 2, 4, 6), delimiter=",", skiprows=1)

plt.xlabel("mpg")
plt.ylabel("hp")

plt.scatter(data[:,0], data[:,2], s=data[:,3] * 25)

plt.show()
