import pandas as pd


cars = pd.read_csv("LV3/mtcars.csv")

# 1
print(cars.sort_values("mpg").head(5))

# 2
print(cars[cars.cyl == 8].sort_values("mpg", ascending=False).head(3))

# 3
print(cars[cars.cyl == 6].mpg.mean())

# 4
print(cars[(cars.cyl == 4) & (cars.wt >= 2.0) & (cars.wt <= 2.2)].mpg.mean())

# 5
print(len(cars[cars.am == 0]), end=", ")
print(len(cars[cars.am == 1]))

# 6
print(len(cars[(cars.am == 1) & (cars.hp > 100)]))

# 7
print(cars.wt.mul(453.6))
