
try:
    ocj = float(input("Ocjena: "))
except:
    print("Unos mora biti broj!")
    exit()

if (ocj < 0 or ocj > 1):
    print("Ocjena mora biti izmedu 0 i 1")
    exit()

if (ocj < 0.6):
    print("F")
elif (ocj < 0.7):
    print("D")
elif (ocj < 0.8):
    print("C")
elif (ocj < 0.9):
    print("B")
else:
    print("A")
