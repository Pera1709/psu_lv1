
try:
    file = open(input("File name: "))
except:
    print("Files doesn't exist!")
    exit()

sum = 0.0
count = 0
for line in file:
    if ("X-DSPAM-Confidence:" in line):
        words = line.split()
        try:
            sum += float(words[-1])
            count += 1
        except:
            print("Wasn't a float!")

print("Average:", sum / count)
