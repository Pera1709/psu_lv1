from re import A
from matplotlib import mlab
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_openml
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.neural_network import MLPClassifier
import joblib
import pickle


X, y = fetch_openml('mnist_784', version=1, return_X_y=True, as_frame=False)

# TODO: prikazi nekoliko ulaznih slika
imgID = 6
plt.figure(y[imgID])
plt.imshow(np.reshape(X[imgID,:], (28, 28)), cmap='gray')
plt.show()

# skaliraj podatke, train/test split
X = X / 255.0
X_train, X_test = X[:60000], X[60000:]
y_train, y_test = y[:60000], y[60000:]

# TODO: izgradite vlastitu mrezu pomocu sckitlearn MPLClassifier 
mlp_mnist = MLPClassifier(hidden_layer_sizes=(100, 100,), activation='relu', max_iter=100, alpha=1e-3, momentum=0.9, solver='sgd', verbose=10, random_state=1, learning_rate_init=0.1)
mlp_mnist.fit(X_train, y_train)

# TODO: evaluirajte izgradenu mrezu
print("Training score: %f" % mlp_mnist.score(X_train, y_train))
print("Test score: %f" % mlp_mnist.score(X_test, y_test))
y_test_pred = mlp_mnist.predict(X_test)
print("Confusion matrix")
print(confusion_matrix(y_test, y_test_pred))
print("Test accuracy")
print(accuracy_score(y_test, y_test_pred))

# spremi mrezu na disk
filename = "LV6/NN_model.sav"
joblib.dump(mlp_mnist, filename)

